import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class MontyHallProcessTest {

    private MontyHallProcess montyHallProcess;

    @BeforeEach
    void setUp() {
        montyHallProcess = new MontyHallProcess();
    }

    @Test
    void runTheGame() {
        String input = "1000";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        assertNotNull(montyHallProcess.runTheGame(),
                "runTheGame method should return list of 2 int, returnd list should not be null.");
    }

    @Test
    void runTheGameIfInputNotInt() {
        String input = "ABC";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        assertEquals(Arrays.asList(0, 0), montyHallProcess.runTheGame(),
                "runTheGame method should return list of 2 int, returnd list should not be null.");
    }


    @Test
    void getIntFromPlayer() {
        String input = "1000";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        assertEquals(1000, montyHallProcess.getInputFromPlayer(),
                "getInputFromPlayer method should return int inserted from the user");
    }

    @Test
    void getNotIntFromPlayer() {
        String input = "ABC";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        assertEquals(0, montyHallProcess.getInputFromPlayer(),
                "getInputFromPlayer method should return int value 0 if user insert non int value");
    }
}