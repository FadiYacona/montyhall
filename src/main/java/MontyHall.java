import java.util.*;

public class MontyHall {

    public static void main(String[] arg) {

        MontyHallProcess montyHallProcess = new MontyHallProcess();

        List<Integer>  listOfProbability = montyHallProcess.runTheGame();

        //Winning probability if not change the door
        System.out.println("Chance to win without changing the door: " + listOfProbability.get(0));

        //Winning probability if change the door
        System.out.println("Chance to win with changing the door:  " + listOfProbability.get(1));
    }
}
