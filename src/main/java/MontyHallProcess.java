import java.util.*;

public class MontyHallProcess {


    /**
     * Run random simulations of the Monty Hall game.
     * Simulate n times games using three doors for each time and return the results as list of Integer to compare the result of each strategy.
     * @return List of Integer
     */
    public List<Integer> runTheGame() {

        Random random = new Random();

        int winWhenChangeTheDoor = 0;
        int winWhenNotChangeTheDoor = 0;
        List<Door> doorList = new ArrayList<Door>();

        //Get input from user.
        int timesPlayed = getInputFromPlayer();

        for (int i = 0; i< timesPlayed; i++) {

            //Start new doors list everyloop.
            doorList.clear();
            doorList.addAll(Arrays.asList(new Door(), new Door(), new Door()));

            //Put the money after a door randomly.
            doorList.get(random.nextInt(3)).setThingBehindTheDoor(ThingBehindTheDoor.MONEY);

            //pick a door randomly.
            int chosenDoor = random.nextInt(3);

            if (doorList.get(chosenDoor).getThingBehindTheDoor().equals(ThingBehindTheDoor.MONEY)){
                winWhenNotChangeTheDoor++;
            } else {
                winWhenChangeTheDoor++;
            }
        }
        List<Integer> listOfWin = Arrays.asList(winWhenNotChangeTheDoor, winWhenChangeTheDoor);
        return listOfWin;
    }


    /**
     * Asking the player to insert a number.
     * If the player insert non int value, function return 0.
     * @return int value that the player insert.
     */
    public int getInputFromPlayer() {
        System.out.println("Hi, how many times do you want to play?");
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (Exception e) {
            return 0;
        }
    }
}
