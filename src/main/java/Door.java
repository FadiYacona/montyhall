public class Door {

    private String thingBehindTheDoor;


    public Door() {
        this.thingBehindTheDoor = ThingBehindTheDoor.EMPTY;
    }

    /**
     * @return the hidden thing behind the door.
     */
    public String getThingBehindTheDoor() {
        return thingBehindTheDoor;
    }

    /**
     * @param thingBehindTheDoor set a hidden thing behind the door.
     */
    public void setThingBehindTheDoor(String thingBehindTheDoor) {
        this.thingBehindTheDoor = thingBehindTheDoor;
    }
}
